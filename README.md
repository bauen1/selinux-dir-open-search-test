# Test for showing why `open` is present in search_dir_perms

1. compile test program `gcc test.c -o test`
2. install policy `make -f /usr/share/selinux/refpolicy/include/Makefile test_module.pp && sudo semodule -i test_module.pp`
3. fix contexts `chcon -t test_domain_exec_t test && chcon -R -t test_dir_t test_dir`
4. run `./test`
