#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

int main(void) {
    int dir_fd = open("test_dir", 0);
    if (dir_fd < 0) {
        perror("open(\"test_dir\")");
        return EXIT_FAILURE;
    }

    int file_fd = openat(dir_fd, "test_file", O_RDONLY);
    if (file_fd < 0) {
        perror("openat(\"test_file\")");
        return EXIT_FAILURE;
    }

    close(file_fd);
    close(dir_fd);

    return EXIT_SUCCESS;
}
